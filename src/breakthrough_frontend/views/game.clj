(ns breakthrough-frontend.views.game
  (:require [breakthrough-frontend.views.common :as common]
  			[breakthrough-frontend.models.games :as game-models])
  (:use [noir.core :only [defpage defpartial]]
  		[hiccup.page :only [include-css include-js]]))

(defn moves-string [game-id]
	(str "\""
		(reduce 
		(fn [one two]
			(str one ";" two))
		(map 
			(fn [item] 
				(get item :move)) 
			(game-models/game-moves game-id)))
		"\""))

(defpage "/game/:id" {:keys [id]}
 (let [moves (moves-string id)]
 	(let [game-info (first (game-models/game-info id))]
	 	(common/layout
		   (include-js "/js/replay.js")
		   [:script {:type "text/javascript"}
		   	(str "window.onload = function() { Replay.loadData(" (moves-string id) ");};")]
		   [:h1.page-title (str "Game #" id)]
		   [:div.row
		    [:div.span5.left-ai.loser [:h4 [:a {:href (str "/ai/" (get game-info :loser))} (get game-info :loser_name)]]]
		    [:div.span2.vs [:h4 "VS"]]
		    [:div.span5.right-ai.winner [:h4 [:a {:href (str "/ai/" (get game-info :winner))} (get game-info :winner_name)]]]]
		   [:div#replay
		    [:div#board
		     [:table#tblBoard]]
		    [:div#controlle
		     [:button#previous.btn "<"]
		     [:button#playPause.btn.btn-primary]
		     [:button#next.btn ">"]]]
		    [:br]
		    [:br]
		    [:div.row
		     [:div.span12.replay-disclaimer 
		      "Replay feature graciously provided by "
		      [:a {:href "https://github.com/HoLyVieR"} "HoLyVieR"]
		      "."]]))))