(ns breakthrough-frontend.views.games-common
  (:require [breakthrough-frontend.views.common :as common])
  (:use [noir.core :only [defpartial]]))

(defpartial game-line [game bg-color]
 [:div {:class (str "row table-row " bg-color)}
  [:div.span1 [:a {:href (str "/game/" (get game :id))} "Replay"]]
  [:div.span4 
   [:a {:href (str "/ai/" (get game :winner))} (get game :winner_name)]]
  [:div.span4
   [:a {:href (str "/ai/" (get game :loser))} (get game :loser_name)]]
  [:div.span3 (get game :date_ended)]])

(defpartial games-table-header []
	[:div.row.table-row.table-header
    [:div.span1 ""]
    [:div.span4 "Winner"]
    [:div.span4 "Loser"]
    [:div.span3 "Timestamp Ended"]])