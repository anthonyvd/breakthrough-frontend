(ns breakthrough-frontend.views.four-oh-four
  (:require [breakthrough-frontend.views.common :as common])
  (:use [noir.statuses :only [set-page!]]))

(set-page! 404 
 (common/layout
   [:div.vertically-centered-404.container
    [:h1 "404 - Page Not Found"]
    [:div.row "Oops!  It appears we can't find that one.  We'll get the IT monkeys to work on locating it.
     In the meantime, use the navigation bar to get around the site."]]))