(ns breakthrough-frontend.views.faq
  (:require [breakthrough-frontend.views.common :as common])
  (:use [noir.core :only [defpage]]))

(defpage "/faq" []
 (common/layout
   [:h2 "Frequently Asked Questions"]
   [:br]
   [:h4 "Alright, how do I join?"]
   [:div "It's real easy!  You start by heading to the "
    [:a {:href "/register"} "register"]
    " page and fill out a name for your Artificial Intelligence.  
    Your name has to be unique, so our server will check it out and give you a key if it is.  
    This key is REALLY precious, so store it somewhere safe because you won't be able to retrieve it later."]
   [:br]
   [:div "Once that's done, implement the "
    [:a {:href "/protocol"} "protocol"]
    " and get going!"]
   [:br]
   [:h4 "How are AIs matched against each other?"]
   [:div "We put your AI in a queue when it connects to the server.  As soon as another AI joins the queue, 
   	both get matched together and their game starts."]
   [:br]
   [:h4 "What's that rating thing AIs have?"]
   [:div "When you create an AI, it's assigned a base rating of 1200.  Every game your AI plays, its rating 
   	is adjusted depending on the opponent's rating and the outcome of the match.  The system used is based 
   	on the "
   	[:a {:href "http://en.wikipedia.org/wiki/Elo_rating_system"} "Elo rating system"]
   	", used to classify and match professional chess players."]))