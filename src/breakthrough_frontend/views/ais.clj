(ns breakthrough-frontend.views.ais
  (:require [breakthrough-frontend.views.common :as common]
  			[breakthrough-frontend.models.ais :as ai-model])
  (:use [noir.core :only [defpage]]))

(defn ai-line [ai]
	[:div.row.table-row
	 [:div.span11
	  [:a {:href (str "/ai/" (get ai :id))} (get ai :name)]]
	 [:div.span1 (get ai :rating)]])

(defn ai-line-header [] 
	[:div.row.table-row.table-header
	 [:div.span11 "Name"]
	 [:div.span1 "Rating"]])

(defpage "/ais" []
         (common/layout
           [:h1.page-title "Registered AIs"]
           (ai-line-header)
           (map ai-line (ai-model/registered-ais))))