(ns breakthrough-frontend.views.protocol
  (:require [breakthrough-frontend.views.common :as common])
  (:use [noir.core :only [defpage]]))

(defpage "/protocol" []
 (common/layout
   [:h2 "Breakthrough protocol"]
   [:div "The protocol described below is heavily inspired from the one used in the "
    [:a {:href "http://www.etsmtl.ca/Programmes-Etudes/1er-cycle/Fiche-de-cours?Sigle=log320"} 
      "Log320 - Algorithms and Data Structures"]
    " class taken by Software Engineering students enrolled at "
    [:a {:href "http://www.etsmtl.ca"} "École de Technologie Supérieure"]
    " in Montreal.  It also aims to maintain best compatibility with the server provided in the course."]
   [:br]
   [:h4 "Connecting to the server"]
   [:div "You connect to the Breakthrough server at app.breakthrough-etsmtl.com on port 8888.
    Once that's done, your program should send its AI key (acquired via the "
      [:a {:href "/register"} "register"]
    " page) and wait for the server to send it the initial game board."]
   [:br]
   [:h4 "Receiving the game board"]
   [:div "Once your AI is matched with an opponent, the server will send them both a 
    65 digit string representing the initial game board.  The first digit will be either 
    1 if your AI plays the white pawns (and thus plays first) or 2 if it plays the black pawns (and plays second)."]
   [:br]
   [:div "The 64 remaining digits are the game board itself.  The board representation starts from the top-left corner, 
    filling horizontal lines of 8 tiles, 8 times.  A 2 represents a Black pawn and a 4 represents a white pawn.  A 0 is an empty tile."]
   [:br]
   [:div "For example, the following string:"]
   [:br]
   [:pre.board-string "1 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4"]
   [:br]
   [:div "would mean this AI plays the white pawns, makes the first move and that the board is intially set up like this:"]
   [:br]
   [:pre.board
     "   ----------------- " [:br]
     "8 | B B B B B B B B |" [:br]
     "7 | B B B B B B B B |" [:br]
     "6 | . . . . . . . . |" [:br]
     "5 | . . . . . . . . |" [:br]
     "4 | . . . . . . . . |" [:br]
     "3 | . . . . . . . . |" [:br]
     "2 | W W W W W W W W |" [:br]
     "1 | W W W W W W W W |" [:br]
     "   ----------------- " [:br]
     "    A B C D E F G H  " [:br]]
    [:br]
    [:div "If your AI plays first, now's the time to send your first move.  Otherwise, you might want to wait for your opponent's move"]
    [:br]
    [:h4 "Sending a move"]
    [:div "When it's your turn, you have 5 seconds to send the 
     server a move using either of the following formats:"]
    [:br]
    [:pre "A1B2" [:br][:br] "or" [:br][:br] "A1 - B2"]
    [:br]
    [:div "Where A1 would be the tile the pawn to move is starting from and B2 the destination tile.  
      If your move is invalid, the server will send you a message string containing only the number 4.  
      This shouldn't happen.  If it does, your AI will eventually (after 3 invalid moves) forcefully forfeit the game.  Make sure you (and your AI!) 
      know the rules of " [:a {:href "http://en.wikipedia.org/wiki/Breakthrough_(board_game)"} "Breakthrough"] " beforehand."]
    [:br]
    [:h4 "Receiving a move from the server"]
    [:div "When your opponent has played a move, the server will relay it to you in this format:"]
    [:br]
    [:pre "3 A1 - B2"]
    [:br]
    [:div "Where 3 means the beginning of the opponent's move message, A1 is the tile the pawn was originally on and B2 its destination tile.
      This move is guaranteed to be valid."]
    [:br]
    [:h4 "Claiming victory or being signaled defeat"]
    [:div.final "Although your AI should be able to tell when it's been defeated or when it has reached victory, the server will send it a 5 in 
      case of victory and a 6 in case of defeat."]))
