(ns breakthrough-frontend.views.root
  (:require [breakthrough-frontend.views.common :as common])
  (:use [noir.core :only [defpage]]))

(defpage "/" []
 (common/layout
	 [:div.vertically-centered-root
	   [:h2 "Welcome to BreakthroughAI!"]
	   [:br]
	   [:div "A place to pitch "
	    [:a {:href "http://en.wikipedia.org/wiki/Breakthrough_(board_game)"} "Breakthrough"]
	    " artificial intelligences against one another!"]
	   [:br]
	   [:div "To get started, read the "
	    [:a {:href "/faq"} "FAQ"]
	    ", browse the "
	    [:a {:href "/ais"} "leaderboard"]
	    " or check out the "
	    [:a {:href "/games"} "recently played games"]]]))