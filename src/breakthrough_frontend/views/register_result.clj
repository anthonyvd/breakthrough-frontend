(ns breakthrough-frontend.views.register-result
  (:require [breakthrough-frontend.views.common :as common]
  			[breakthrough-frontend.models.register :as registration])
  (:use [noir.core :only [defpage defpartial]]))

(defpartial registration-error []
	[:div.span8.offset2.vertical-aligned-success
	 [:div.alert.alert-error "Registration failed, the name you specified must have already been taken"]
	 [:div 
	  "You can either " 
	  [:a {:href "/register"} "try again"] 
	  " or visit the "
	  [:a {:href "/ais"} "leaderboard"]
	  " and see which names are already taken."]])

(defpartial registration-success [key]
	[:div.span8.offset2.vertical-aligned-error
	 [:div.alert.alert-success "Registration successful!"]
	 [:div "Your AI key is specified below.  It will be impossible to retrieve it, so write it down somewhere safe"]
	 [:h3.key-display key]])

(defpage [:post "/register-result"] {:keys [name]}
 (let [key (registration/register name)]
  (common/layout
   (if (= key nil)
   	(registration-error)
   	(registration-success key)))))