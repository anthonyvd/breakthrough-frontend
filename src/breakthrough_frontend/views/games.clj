(ns breakthrough-frontend.views.games
  (:require [breakthrough-frontend.views.common :as common]
  			[breakthrough-frontend.models.games :as games-model]
  			[breakthrough-frontend.views.games-common :as games-common])
  (:use [noir.core :only [defpage defpartial]]))

(defpage "/games" []
 (common/layout
   [:h1.page-title "Recent games"]
   (games-common/games-table-header)
   (map games-common/game-line (games-model/fetch-games) "white-row")))