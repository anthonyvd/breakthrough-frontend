(ns breakthrough-frontend.views.common
  (:use [noir.core :only [defpartial]]
        [hiccup.page :only [include-css include-js html5]]))

(defn navbar []
	[:div.navbar.navbar-inverse.navbar-fixed-top
     [:div.navbar-inner
      [:div.container
       [:a.brand {:href "/"} "BreakthroughAI"]
       [:div.nav-collapse.collapse
        [:ul.nav
         [:li [:a {:href "/ais"} "Leaderboard"]]
         [:li [:a {:href "/games"} "Recent games"]]
         [:li [:a {:href "/register"} "Register"]]
         [:li [:a {:href "/protocol"} "Protocol"]]
         [:li [:a {:href "/faq"} "FAQ"]]
         ]]]]])

(defpartial layout [& content]
            (html5
              [:head
               [:title "BreakthroughAI"]
               (include-css "/css/bootstrap.min.css")
               (include-css "/css/breakthrough.css")]
               (include-css "/css/replay.css")
              [:body
               [:div#wrap
                (navbar)
                [:div#root.container content]
                [:div#push]]
               [:div#footer
                [:div.container
                 [:p "Made by avd using "
                  [:a {:href "http://clojure.org/"} "Clojure"]
                  ", "
                  [:a {:href "http://www.webnoir.org/"} "Noir"]
                  " and "
                  [:a {:href "http://twitter.github.com/bootstrap/"} "Bootstrap"]
                  "."]]]
                (include-js "/js/bootstrap.min.js")]))