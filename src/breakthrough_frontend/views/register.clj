(ns breakthrough-frontend.views.register
  (:require [breakthrough-frontend.views.common :as common])
  (:use [noir.core :only [defpage defpartial]]))

(defpartial register-form []
	[:div.offset3.span5.vertical-aligned-form
	 [:form {:method "post" :action "register-result"}
	  [:fieldset
	   [:legend "Register a new AI"]
	   [:label "AI name"]
	   [:input.input-xlarge {:type "text" :placeholder "HAL, Skynet, Wheaton, GlaDOS..." :name "name"}]
	   [:button.btn.btn-primary.pull-right {:type "submit"} "Submit"]
	   [:span.help-block "The name of your AI must be unique"]]]])

(defpage "/register" []
 (common/layout
   (register-form)))