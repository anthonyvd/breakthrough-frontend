(ns breakthrough-frontend.views.five-oh-oh
  (:require [breakthrough-frontend.views.common :as common])
  (:use [noir.statuses :only [set-page!]]))

(set-page! 500 
 (common/layout
  [:div.vertically-centered-505.container
   [:h1 "500 - Internal Server Error"]
   [:div.row "Looks like our programmers didn't drink enough coffee and let some bugs slip.
   	We'll make sure to whip them twice as hard next time.  In the meantime, you can use the navigation bar to get around the site."]]))