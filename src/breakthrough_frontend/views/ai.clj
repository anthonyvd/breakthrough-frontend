(ns breakthrough-frontend.views.ai
  (:require [breakthrough-frontend.views.common :as common]
  			[breakthrough-frontend.models.ai :as ai-model]
  			[breakthrough-frontend.models.games :as games-model]
  			[breakthrough-frontend.views.games-common :as games-common])
  (:use [noir.core :only [defpage defpartial]]))

(defpartial game-line [game ai-id]
	(games-common/game-line game (if (= (get game :winner) ai-id) "green-row" "red-row")))

(defpage "/ai/:id" {:keys [id]}
 (let [the-ai (first (ai-model/ai-with-id id))]
  (common/layout
   [:h1.page-title (get the-ai :name)]
   [:div.row [:h3 (str "Rating: " (get the-ai :rating))]]
   [:div.row [:h3 "Games played"]]
   (games-common/games-table-header)
   (map 
   	(fn [game] (game-line game (get the-ai :id))) 
   	(games-model/games-for-ai id)))))
