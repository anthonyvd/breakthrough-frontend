(ns breakthrough-frontend.models.games
	(:require [clojure.java.jdbc :as sql]
			  [breakthrough-frontend.models.database :as db]))

(defn fetch-games []
	(sql/with-connection db/db-connection
		(sql/with-query-results rs 
			["SELECT 
				g.id, 
				g.date_ended, 
				g.winner, 
				g.loser, 
				winner.name AS winner_name, 
				loser.name AS loser_name 
					FROM games as g 
					JOIN ais as winner ON g.winner=winner.id 
					JOIN ais as loser ON g.loser=loser.id
				ORDER BY g.date_ended DESC
				LIMIT 0, 20"]
			(doall rs))))

(defn games-for-ai [ai-id]
	(sql/with-connection db/db-connection
		(sql/with-query-results rs
			["SELECT 
				g.id, 
				g.date_ended, 
				g.winner, 
				g.loser, 
				winner.name AS winner_name, 
				loser.name AS loser_name 
					FROM games as g 
					JOIN ais as winner ON g.winner=winner.id 
					JOIN ais as loser ON g.loser=loser.id
				WHERE g.winner=? OR g.loser=?
				ORDER BY g.date_ended DESC" ai-id ai-id]
			(doall rs))))

(defn game-info [game-id]
	(sql/with-connection db/db-connection
		(sql/with-query-results rs
			["SELECT 
				g.id, 
				g.date_ended, 
				g.winner, 
				g.loser, 
				winner.name AS winner_name, 
				loser.name AS loser_name 
					FROM games as g 
					JOIN ais as winner ON g.winner=winner.id 
					JOIN ais as loser ON g.loser=loser.id
				WHERE g.id=?" game-id]
			(doall rs))))

(defn game-moves [game-id]
	(sql/with-connection db/db-connection
		(sql/with-query-results rs
			["SELECT move, turn
			  FROM game_moves
			  WHERE game_id=?
			  ORDER BY turn ASC" game-id]
			(doall rs))))