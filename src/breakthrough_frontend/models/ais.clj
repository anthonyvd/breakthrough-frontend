(ns breakthrough-frontend.models.ais
	(:require [clojure.java.jdbc :as sql]
			  [breakthrough-frontend.models.database :as db]))

(defn registered-ais []
	(sql/with-connection db/db-connection
		(sql/with-query-results rs ["select id, name, rating from ais order by ais.rating desc"]
			(doall rs))))