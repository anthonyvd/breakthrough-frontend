(ns breakthrough-frontend.models.register
	(:import  [java.util.UUID])
	(:require [clojure.java.jdbc :as sql]
			  [breakthrough-frontend.models.database :as db]))

(defn ai-for-name [name]
	(sql/with-connection db/db-connection
		(sql/with-query-results rs ["select id from ais where ais.name=?" name]
			(doall rs))))

(defn create-new-ai [name]
	(let [uid (.toString (java.util.UUID/randomUUID))]
		(let [result 
				(sql/with-connection db/db-connection
					(sql/insert-values :ais
						[:name :ai_key :rating]
						[name uid 1200]))]
			uid)))

(defn register [name]
	(if (not (= 0 (count (ai-for-name name))))
	 nil
	 (create-new-ai name)))