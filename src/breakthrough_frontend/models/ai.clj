(ns breakthrough-frontend.models.ai
	(:require [clojure.java.jdbc :as sql]
			  [breakthrough-frontend.models.database :as db]))

(defn ai-with-id [id]
	(sql/with-connection db/db-connection
		(sql/with-query-results rs ["select id, name, rating from ais where ais.id=?" id]
			(doall rs))))
